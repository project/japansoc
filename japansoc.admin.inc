<?php


/**
 * @file
 * Administration include file.
 */

/**
 * Returns form to be displayed on module's settings page.
 *
 * @return
 *   array A form processed by system_settings_form().
 */
function japansoc_settings_form()
{

  // Check if filter is enabled.  This portion is based on code from the Inline module.
  $filter_enabled = FALSE;
  foreach(filter_formats() as $format)
  {
    foreach(filter_list_format($format->format) as $filter)
    {
      if($filter->module == 'japansoc')
      {
        $filter_enabled = TRUE;
        break 2;
      }
    }
  }
  if($filter_enabled == FALSE)
  {
    drupal_set_message(t('The JapanSoc filter is not yet enabled for at least one <a href="!formats">input format</a>.', array('!formats' => url('admin/settings/filters'))), 'warning');
  }
  
	$form = array();
	$types = node_get_types('names');
	
	$form['japansoc_script_url'] = array(
		'#type' => 'textfield',
		'#title' => t('Button script URL'),
		'#description' => t('The URL of the dynamic JapanSoc "Soc It!" button JavaScript file. You probably do not need to change this.'),
		'#default_value' => variable_get('japansoc_script_url', JAPANSOC_SCRIPT_URL_DEFAULT),
	);
	
	$form['japansoc_node_types'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Automatically add "Soc It!" button to nodes of these types'),
		'#description' => t('If not inserted manually, the "Soc It!" button will be inserted automatically to nodes of the types selected.'),
		'#default_value' => variable_get('japansoc_node_types', array()),
		'#options' => $types,
	);
	
	$form['japansoc_button_weight'] = array(
	  '#type' => 'weight',
	  '#title' => t('Weight'),
	  '#default_value' => variable_get('japansoc_button_weight', JAPANSOC_WEIGHT_DEFAULT),
	  '#delta' => 50,
	  '#description' => t("When inserted automatically, the heavier the button is, the lower it will be displayed in the node's body."),
	);
	
	$form['japansoc_float_default'] = array(
	  '#type' => 'select',
	  '#title' => t('Default float'),
	  '#default_value' => variable_get('japansoc_float_default', JAPANSOC_FLOAT_DEFAULT),
	  '#options' => array('nofloat' => t('No float'), 'left' => t('Left'), 'right' => t('Right')),
	  '#description' => t("Use this floating position when not defined in the filter code (i.e. when using '[japansoc]')."),
	);
	
	return system_settings_form($form);
	
}

